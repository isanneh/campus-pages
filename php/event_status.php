<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
		
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	
	//Sanitize the POST values

$status = clean($_POST['status']);
$prev_status = clean($_POST['prev_status']);
$id = clean($_POST['id']);

$url .="event_page.php?id=";
$url .=$id;

	//Input Validations


if($status == 'choose status') {
		$errmsg_arr[] = 'No status chosen!';
		$errflag = true;
	}

if($status == $prev_status) {
		$msg='This event has already been ';
		$msg .= $prev_status;
		$errmsg_arr[] = $msg;
		$errflag = true;
	}
	
	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		$header_url = "location:";
		$header_url .= $url;
		header($header_url);
		exit();
	}


$qry_log="INSERT into log (category, approved_by, url, status) VALUES ('events', '".$_SESSION['SESS_EMAILADDRESS']."', '$url', '$status')";
$result_log=mysql_query($qry_log);
if(! $result_log)
{
 die ("log query failed!");
}




	
//Create query
$qry="UPDATE `events` SET `status`='$status'WHERE id='$id'";

$result=@mysql_query($qry);
if($result)
{
header("location: events2.php");
exit();
}
else
{
die("query failed32");
}
			

?>
