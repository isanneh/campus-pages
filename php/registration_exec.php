<?php
	//Start session
	session_start();
	
	//Include database connection details
	//require_once('connect.php');
	include("connect.php");
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

//email format
	function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

//name format

function isValidName($name){
return eregi ("^[a-zA-Z][a-zA-Z -]*$", $name);
}


	
//Sanitize the POST values

$first_name = clean($_POST['first_name']);
$last_name = clean($_POST['last_name']);
$gender = clean($_POST['gender']);
$school = clean($_POST['school']);
$email_address = clean($_POST['email_address']);
$password = clean($_POST['password']);
$password2 = clean($_POST['password2']);	
$phone = clean($_POST['phone']);


	
//Input Validations
if($first_name == '') {
	$errmsg_arr[] = 'First name missing';
	$errflag = true;
}

//Input Validations
if($last_name == '') {
	$errmsg_arr[] = 'Last name missing';
	$errflag = true;
}

if($gender == 'choose'  ) {
		$errmsg_arr[] = 'Gender missing';
		$errflag = true;
	}



if($school == 'choose school') {
		$errmsg_arr[] = 'School missing';
		$errflag = true;
	}


if($email_address == '') {
	$errmsg_arr[] = 'Email missing';
	$errflag = true;
}
else
{
if(isValidEmail($email_address) ==TRUE)
{
}
else
{
$errmsg_arr[] = 'Email address is not valid';
		$errflag = true;
}
}
	
if($password == '') {
	$errmsg_arr[] = 'Password missing';
	$errflag = true;
}
if($password2 == '') {
	$errmsg_arr[] = 'Confirm password missing';
	$errflag = true;
}
if( strcmp($password, $password2) != 0 ) {
	$errmsg_arr[] = 'Passwords do not match';
	$errflag = true;
}

	



	
//Check for duplicate login ID
if($email_address != '') {
	$qry = "SELECT * FROM users WHERE email_address='$email_address'";
	$result = mysql_query($qry);
	if($result) {
		if(mysql_num_rows($result) > 0) {
			$errmsg_arr = array();
			$errmsg_arr[] = 'Email Address already in use';
			$errflag = true;
		}
		@mysql_free_result($result);
	}
	else {
		die("Query failed");
	}
}

//If there are input validations, redirect back to the registration form
if($errflag) {
	$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
	session_write_close();
	header("location: register.php");
	exit();
}


	//Create INSERT query
$qry= "INSERT INTO users(first_name, last_name, school, email_address, gender, password, phone) VALUES ('$first_name', '$last_name', '$school', '$email_address', '$gender', '".md5($_POST['password'])."', '$phone')";

$result = @mysql_query($qry);

//Check whether the query was successful or not
if($result) {
		$_SESSION['SESS_EMAILADDRESS'] = $email_address;

	header("location: registration_success.php");
	exit();
}else {
	die("Query failed");
}
?>
