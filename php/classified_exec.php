<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	/*//Connect to mysql server
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	if(!$link) {
		die('Failed to connect to server: ' . mysql_error());
	}


	//Select database
	$db = mysql_select_db(DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}
*/

	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}


	
//Sanitize the POST values
	
$title = clean($_POST['title']);
$category = clean($_POST['category']);
$sale_details = clean($_POST['sale_details']);
$sale_amount = clean($_POST['sale_amount']);
$sale_person = clean($_POST['sale_person']);
$sale_contact_email = clean($_POST['sale_contact_email']);
$sale_phone = clean($_POST['sale_phone']);


$housing_wanted_details = clean($_POST['housing_wanted_details']);
$housing_wanted_amount = clean($_POST['housing_wanted_amount']);
$housing_wanted_person = clean($_POST['housing_wanted_person']);
$housing_wanted_contact_email = clean($_POST['housing_wanted_contact_email']);
$housing_wanted_phone = clean($_POST['housing_wanted_phone']);
$housing_wanted_duration = clean($_POST['housing_wanted_duration']);

$housing_available_details = clean($_POST['housing_available_details']);
$housing_available_amount = clean($_POST['housing_available_amount']);
$housing_available_person = clean($_POST['housing_available_person']);
$housing_available_contact_email = clean($_POST['housing_available_contact_email']);
$housing_available_phone = clean($_POST['housing_available_phone']);
$housing_available_duration = clean($_POST['housing_available_duration']);

$job_details = clean($_POST['job_details']);
$job_amount = clean($_POST['job_amount']);
$job_person = clean($_POST['job_person']);
$job_contact_email = clean($_POST['job_contact_email']);
$job_phone = clean($_POST['job_phone']);
$job_website = clean($_POST['job_website']);

$internship_details = clean($_POST['internship_details']);
$internship_amount = clean($_POST['internship_amount']);
$internship_person = clean($_POST['internship_person']);
$internship_contact_email = clean($_POST['internship_contact_email']);
$internship_phone = clean($_POST['internship_phone']);
$internship_duration = clean($_POST['internship_duration']);
$internship_month = clean($_POST['internship_month']);
$internship_day = clean($_POST['internship_day']);
$internship_year = clean($_POST['internship_year']);
$internship_check_deadline = clean($_POST['internship_check_deadline']);


$reu_details = clean($_POST['reu_details']);
$reu_amount = clean($_POST['reu_amount']);
$reu_person = clean($_POST['reu_person']);
$reu_contact_email = clean($_POST['reu_contact_email']);
$reu_phone = clean($_POST['reu_phone']);
$reu_duration = clean($_POST['reu_duration']);
$reu_month = clean($_POST['reu_month']);
$reu_day = clean($_POST['reu_day']);
$reu_year = clean($_POST['reu_year']);
$reu_check_deadline = clean($_POST['reu_check_deadline']);

$services_details = clean($_POST['services_details']);
$services_amount = clean($_POST['services_amount']);
$services_person = clean($_POST['services_person']);
$services_contact_email = clean($_POST['services_contact_email']);
$services_phone = clean($_POST['services_phone']);
$services_website = clean($_POST['services_website']);

$tutoring_details = clean($_POST['tutoring_details']);
$tutoring_amount = clean($_POST['tutoring_amount']);
$tutoring_person = clean($_POST['tutoring_person']);
$tutoring_contact_email = clean($_POST['tutoring_contact_email']);
$tutoring_phone = clean($_POST['tutoring_phone']);


$textbooks_wanted_details = clean($_POST['textbooks_wanted_details']);
$textbooks_wanted_amount = clean($_POST['textbooks_wanted_amount']);
$textbooks_wanted_person = clean($_POST['textbooks_wanted_person']);
$textbooks_wanted_contact_email = clean($_POST['textbooks_wanted_contact_email']);
$textbooks_wanted_phone = clean($_POST['textbooks_wanted_phone']);


$textbooks_for_sale_details = clean($_POST['textbooks_for_sale_details']);
$textbooks_for_sale_amount = clean($_POST['textbooks_for_sale_amount']);
$textbooks_for_sale_person = clean($_POST['textbooks_for_sale_person']);
$textbooks_for_sale_contact_email = clean($_POST['textbooks_for_sale_contact_email']);
$textbooks_for_sale_phone = clean($_POST['textbooks_for_sale_phone']);






	
	//Input Validations


if($category == 'choose_category') {
		$errmsg_arr[] = 'No category chosen!';
		$errflag = true;
	}


if($title == '') {
		$errmsg_arr[] = 'No title added!';
		$errflag = true;
	}

if($category == 'for sale') {

if($sale_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($sale_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}

}



else if($category == 'housing wanted') {

if($housing_wanted_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($housing_wanted_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}

}

else if($category == 'housing available') {

if($housing_available_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($housing_available_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}



}

else if($category == 'job') {

if($job_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}



}

else if($category == 'internship') {

if($internship_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}



if (isset($_POST['internship_check_deadline']))
	{
	$internship_month="";
	$internship_day="";
	$internship_year="";
	}
else
	{
	$num=0;
	if($internship_month == '') {
			$errmsg_arr[] = 'No month added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($internship_day == '') {
			$errmsg_arr[] = 'No day added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($internship_year == '') {
			$errmsg_arr[] = 'No year added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($num != 0)
		{
			$errmsg_arr[] = 'No deadline added!';
			$errflag = true;
		}
	}

}


else if($category == 'reu') {

if($reu_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}

if (isset($_POST['reu_check_deadline']))
	{
	$reu_month="";
	$reu_day="";
	$reu_year="";
	}
else
	{
	$num=0;
	if($reu_month == '') {
			$errmsg_arr[] = 'No month added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($reu_day == '') {
			$errmsg_arr[] = 'No day added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($reu_year == '') {
			$errmsg_arr[] = 'No year added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($num != 0)
		{
			$errmsg_arr[] = 'No deadline added!';
			$errflag = true;
		}
	}

}

else if($category == 'services') {

if($services_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($services_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


}

else if($category == 'tutoring') {

if($tutoring_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($tutoring_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


}

else if($category == 'textbooks wanted') {

if($textbooks_wanted_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($textbooks_wanted_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


}

else if($category == 'textbooks for sale') {

if($textbooks_for_sale_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($textbooks_for_sale_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


}







	

	
	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: classified.php");
		exit();
	}

if($category == 'for sale')
{

	
//Create query
$qry="INSERT INTO `for_sale`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `details`, `status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$sale_contact_email', '$sale_phone', '$sale_person', '$sale_amount', '$sale_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'housing wanted')
{

	
//Create query
$qry="INSERT INTO `housing_wanted`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$housing_wanted_contact_email', '$housing_wanted_phone', '$housing_wanted_person', '$housing_wanted_amount', '$housing_wanted_duration',  '$housing_wanted_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'housing available')
{

	
//Create query
$qry="INSERT INTO `housing_available`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`, `details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$housing_available_contact_email', '$housing_available_phone', '$housing_available_person', '$housing_available_amount', '$housing_available_duration',  '$housing_available_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'job')
{

	
//Create query
$qry="INSERT INTO `job`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `website`,  `details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$job_contact_email', '$job_phone', '$job_person', '$job_website',  '$job_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'internship')
{

	
//Create query
$qry="INSERT INTO `internship`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`,`details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$internship_contact_email', '$internship_phone', '$internship_person', '$internship_website', '$internship_month', '$internship_day', '$internship_year',  '$internship_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'reu')
{

	
//Create query
$qry="INSERT INTO `reu`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`,  `details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$reu_contact_email', '$reu_phone', '$reu_person', '$reu_website', '$reu_month', '$reu_day', '$reu_year',  '$reu_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'services')
{

	
//Create query
$qry="INSERT INTO `services`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `website`, `details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$services_contact_email', '$services_phone', '$services_person', '$services_amount', '$services_website',  '$services_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'tutoring')
{

	
//Create query
$qry="INSERT INTO `tutoring`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`,  `details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$tutoring_contact_email', '$tutoring_phone', '$tutoring_person',  '$tutoring_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'textbooks wanted')
{

	
//Create query
$qry="INSERT INTO `textbooks_wanted`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$textbooks_wanted_contact_email', '$textbooks_wanted_phone', '$textbooks_wanted_person',  '$textbooks_wanted_amount', '$textbooks_wanted_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'textbooks for sale')
{

	
//Create query
$qry="INSERT INTO `textbooks_for_sale`(`school`, `submitter_email`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `details`,`status`) 
VALUES ('".$_SESSION['SESS_SCHOOL']."', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$textbooks_for_sale_contact_email', '$textbooks_for_sale_phone', '$textbooks_for_sale_person',  '$textbooks_for_sale_amount',   '$textbooks_for_sale_details', 'pending')"; 


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}






			

?>
