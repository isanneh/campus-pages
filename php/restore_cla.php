<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;

	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	//Sanitize the POST values

	$id = clean($_POST['id']);
	$version = clean($_POST['version']);
	$category = clean($_POST['category']);
	
	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: classified.php");
		exit();
	}

if($category == 'for sale')
{


	$qry_current_data="SELECT * FROM for_sale_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

			//Create query
			$qry="UPDATE `for_sale` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$sale_contact_email', `phone`='$sale_phone', `name`='$sale_person', `amount`='$sale_amount',  `details`='$sale_details' WHERE id='$id'";



			$result=@mysql_query($qry);
			if($result)
			{

				$info	= "restored version ";
				$info .= $version;


					$qry_copy="INSERT INTO for_sale_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`,  `check_email`, `check_phone`, `details`, `status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`,  `check_email`, `check_phone`, `details`, `status`, 'original version' FROM for_sale WHERE id='$id'";
					$result_copy=mysql_query($qry_copy);
					if(! $result_copy)
					{
					die("copy query failed!");
					}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


		

else if($category == 'housing wanted')
{

	$qry_current_data="SELECT * FROM housing_wanted_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);
	
//Create query
$qry="UPDATE `housing_wanted` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$housing_wanted_contact_email', `phone`='$housing_wanted_phone', `name`='$housing_wanted_person', `amount`='$housing_wanted_amount', `duration`='$housing_wanted_duration', `details`='$housing_wanted_details' WHERE id='$id'";





	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;

			$qry_copy="INSERT INTO housing_wanted_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`,`status`, 'original version' FROM housing_wanted WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


else if($category == 'housing available')
{

	$qry_current_data="SELECT * FROM housing_available_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

//Create query

$qry="UPDATE `housing_available` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$housing_available_contact_email', `phone`='$housing_available_phone', `name`='$housing_available_person', `amount`='$housing_available_amount', `duration`='$housing_available_duration', `details`='$housing_available_details' WHERE id='$id'";




	

	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;


			$qry_copy="INSERT INTO housing_available_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`,`status`, 'original version' FROM housing_available WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


else if($category == 'job')
{

	$qry_current_data="SELECT * FROM job_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

//Create query

$qry="UPDATE `job` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$job_contact_email', `phone`='$job_phone', `name`='$job_person', `website`='$job_website',  `details`='$job_details' WHERE id='$id'";



	


	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;



			$qry_copy="INSERT INTO job_edits (`id`, `school`, `edited_`school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_`school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `check_email`, `check_phone`, `details`, 'original version' FROM job WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


else if($category == 'internship')
{

	$qry_current_data="SELECT * FROM internship_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

//Create query

$qry="UPDATE `internship` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$internship_contact_email', `phone`='$internship_phone', `name`='$internship_person', `website`='$internship_website', `month`='$internship_month', `day`='$internship_day', `year`='$internship_year', `details`='$internship_details' WHERE id='$id'";



	

	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;


			$qry_copy="INSERT INTO internship_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`, 'original version' FROM internship WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


else if($category == 'reu')
{

	$qry_current_data="SELECT * FROM reu_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

//Create query

$qry="UPDATE `reu` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$reu_contact_email', `phone`='$reu_phone', `name`='$reu_person', `website`='$reu_website', `month`='$reu_month', `day`='$reu_day', `year`='$reu_year', `details`='$reu_details' WHERE id='$id'";





	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;


			$qry_copy="INSERT INTO reu_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`,`status`, 'original version' FROM reu WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


else if($category == 'services')
{

	$qry_current_data="SELECT * FROM services_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

//Create query


$qry="UPDATE `services` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$services_contact_email', `phone`='$services_phone', `name`='$services_person', `website`='$services_website', `details`='$services_details' WHERE id='$id'";



	


	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;


			$qry_copy="INSERT INTO services_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `website`, `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `website`, `check_email`, `check_phone`, `details`,`status`, 'original version' FROM services WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


else if($category == 'tutoring')
{


	$qry_current_data="SELECT * FROM tutoring_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

//Create query

$qry="UPDATE `tutoring` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$tutoring_contact_email', `phone`='$tutoring_phone', `name`='$tutoring_person', `details`='$tutoring_details' WHERE id='$id'";



	


	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;


			$qry_copy="INSERT INTO tutoring_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `check_email`, `check_phone`, `details`,`status`, 'original version' FROM tutoring WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


else if($category == 'textbooks wanted')
{


	$qry_current_data="SELECT * FROM textbooks_wanted_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

//Create query

$qry="UPDATE `textbooks_wanted` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$textbooks_wanted_contact_email', `phone`='$textbooks_wanted_phone', `name`='$textbooks_wanted_person', `details`='$textbooks_wanted_details' WHERE id='$id'";


	


	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;
			$qry_copy="INSERT INTO textbooks_wanted_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`,`status`, 'original version' FROM textbooks_wanted WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}


else if($category == 'textbooks for sale')
{


	$qry_current_data="SELECT * FROM textbooks_for_sale_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$row=mysql_fetch_assoc($result_current_data);

//Create query

$qry="UPDATE `textbooks_for_sale` SET `school`='$id', 'ccny', `title`='$title', `category`='$category', `contact_email`='$textbooks_for_sale_contact_email', `phone`='$textbooks_for_sale_phone', `name`='$textbooks_for_sale_person', `details`='$textbooks_for_sale_details' WHERE id='$id'";


	


	$result=@mysql_query($qry);
	if($result)
	{
		$info	= "restored version ";
		$info .= $version;


			$qry_copy="INSERT INTO textbooks_for_sale_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`,`status`, `info`) SELECT `id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`,`status`, 'original version' FROM textbooks_for_sale WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

					header("location: events2.php");
					exit();

			}

			else

			{
				die("query restore update failed");
			}
	
		}
	}
}






			

?>
