<?php
	session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="twitter-bootstrap-d991ef2/docs/assets/css/bootstrap.css" rel="stylesheet">  
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="twitter-bootstrap-d991ef2/docs/assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="new.css" rel="stylesheet" type="text/css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../../../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../../../assets/ico/favicon.png">

<?php


if ( ($_SESSION['SESS_STATUS'] == 'member') || ($_SESSION['SESS_STATUS'] == 'admin') || ($_SESSION['SESS_STATUS'] == 'superadmin'))
{

$name= $_SESSION['SESS_FIRSTNAME'];
$name.= ' ';
$name.= $_SESSION['SESS_LASTNAME'];
}

?>

<script type="text/javascript">

function FillContactDetails(contact) 
{

	if(contact.contact_check.checked == true)
	{
	//contact.contact_email.value=contact.email.value;
	var name = "<?php echo $name; ?>";
	var email = "<?php echo $_SESSION['SESS_EMAILADDRESS']; ?> "; 			 
	var contact_phone = "<?php echo $_SESSION['SESS_PHONE']; ?> ";
	contact.person.value=name;
	contact.contact_email.value= email;
	contact.phone.value=contact_phone;

  	}
}


function addAmount2(amount_num)
{
	var issue1= document.getElementById('myDiv');
	var text=document.createElement('div1'); 

	text.innerHTML='<th>Amount: ($) </th><td><input name="amount" type="text" class="textfield" id="amount" ></td>';


	if(amount_num.admission.value == '1')
	{

	issue1.innerHTML='<tr id="myDiv"><th>Amount: ($) </th><td><input name="amount" type="text" class="textfield" id="amount" ></td></tr>';

	}
	
	
	

	if(amount_num.admission.value == '0')
	{

	issue1.removeChild(issue1.firstChild);
	issue1.removeChild(issue1.firstChild);
	}
}

function addAmount()
{

var issue1= document.getElementById('myDiv');
var text=document.createElement('div1'); 
text.innerHTML='<p> Issue: </p><textarea name="description_short" id="description_short"  ></textarea>';
issue1.appendChild(text);


}



function displayDate()
{
document.getElementById("demo").innerHTML=Date();
}

function AmountForm(amount_num) 
{
	if(amount_num.admission.value == '0')
	{
	document.getElementById("add").innerHTML=Date();
  	}
}

function addSchool(school_element)
    {
	if (school_element.school.value != 'choose')
	{
		//parent element, i.e table
		var parent = document.getElementById("submissionContainer");
		//element before school dropdown menu, selected schools will be displayed above this row
		var department_row = document.getElementById("department_row");
		//get row index
		var department_row_index = department_row.rowIndex;
		//insert new row
		var new_row = parent.insertRow(department_row_index);
		//add id to row
		//new_row.id = dates_index.toString();
		//create two cells, e.g. td
		var cell1 = new_row.insertCell(0);
		var cell2 = new_row.insertCell(1);
		var cell3 = new_row.insertCell(2);
		//adding school name to cell2 (2nd column)
		cell2.innerHTML = school_element.school.value + '<input type = "hidden" name = "school_list[]" value = "' +  school_element.school.value + '"/' + '>';
		//cell2.name = 'school_list[]';
		//button to remove added school
		cell3.innerHTML = '<input type = "button" value = "remove" onClick = "removeSchool(this)"/' + '>';
	}
    }


function addDepartment(department_element)
    {
	if (department_element.department.value != 'choose')
	{
		//parent element, i.e table
		var parent = document.getElementById("submissionContainer");
		//element before school dropdown menu, selected departments will be displayed above this row
		var dates = document.getElementById("dates");
		//get row index
		var dates_index = dates.rowIndex;
		//insert new row
		var new_row = parent.insertRow(dates_index);
		//add id to row
		//new_row.id = dates_index.toString();
		//create two cells, e.g. td
		var cell1 = new_row.insertCell(0);
		var cell2 = new_row.insertCell(1);
		var cell3 = new_row.insertCell(2);
		//adding department name to cell2 (2nd column)
		cell2.innerHTML = department_element.department.value + '<input type = "hidden" name = "department_list[]" value = "' +  department_element.department.value + '"/' + '>'; 
		//cell2.name = 'department_list[]';
		//button to remove added school
		cell3.innerHTML = '<input type = "button" value = "remove" onClick = "removeSchool(this)"/' + '>';
	}
    }


function removeSchool(element)
    {
	//get row
	var row = element.parentNode.parentNode;
	//get row id
	//var row_id = row.id;
	// get row_index
	var row_index = row.rowIndex;
	//get table element
	var table = row.parentNode;
	//remove row
	table.deleteRow(row_index);
    }

function addAudience(element)
    {

	var school_row = document.getElementById("school_row");
	var department_row = document.getElementById("department_row");

	if(element.open.value == 'all')
	{
	    school_row.style.display="none";
	    department_row.style.display="none";
	}

	else if(element.open.value == 'school_row')
	{
	    school_row.style.display="table-row";
	    department_row.style.display="none";
	}

	else if(element.open.value == 'department_row')
	{
	    department_row.style.display="table-row";
	    school_row.style.display="none";
	}

    }



</script>

  </head>

  <body>
<?php
include('promotion.php');
?>

<div class="container-fluid">
<div class="row-fluid">
  <div class="span12">
  <!--menu-->
<?php
include('menu.php');
?>
  
</div>
  </div>
<div class="row-fluid">
  <div class="span2">
  <!--sidebar menu-->
<?php
include('sidebar_menu.php');
?>
  </div>
  <div class="span2">
  <!--filter-->

  </div>
  <div class="span6">
  <!--content-->
<?php


if ( ($_SESSION['SESS_STATUS'] == 'member') || ($_SESSION['SESS_STATUS'] == 'admin') || ($_SESSION['SESS_STATUS'] == 'superadmin'))
{

/*$name= $_SESSION['SESS_FIRSTNAME'];
$name.= ' test';
$name.= $_SESSION['SESS_LASTNAME'];*/

/*$member_qry= "SELECT * from users WHERE email_address='.$_SESSION['SESS_EMAILADDRESS'].' ";
$member_result = mysql_query($member_qry);
if($member_result)
{
$member_row = mysql_fetch_assoc($member_result);
}
else
{
die ("Query failed!");
}
*/
$result_pro=mysql_query($qry_pro);
if($result_pro)
{
	

	if(mysql_num_rows($result_pro) != 0)
	{
?>

		


			

		<b style="color:#0196e3";> Promoted Events: </b>	
		<?php
			$row_pro=mysql_fetch_assoc($result_pro);
			$url="event_page.php?id=";
			$url .=$row_pro['id'];

			echo '	<div id="submissionPromotion" style="color:#0196e3";>
					<b #0196e3><a href="'.$url.'">  '.$row_pro['title'].' @ '.$row_pro['location'].' from '.$row_pro['start_hour'].':'.$start.' '.$row_pro['start'].' to '.$row_pro['end_hour'].':'.$end.' '.$row_pro['end'].'</a></b>
			<br/>
			'.$row_pro['details'].'
			<br/>'; 
			foreach ($extra as $i)
				{
					echo ' *'.$i.'* ';
				}
	}
}
else
{
die("promotion query failed!");
}

?>



<?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		echo '<ul class="err">';
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo '<li>',$msg,'</li>'; 
		}
		echo '</ul>';
		unset($_SESSION['ERRMSG_ARR']);
	}
?>


<form id="loginForm" name="loginForm" method="post" action="event_exec.php">
  <table width="100%" border="0" align="left" cellpadding="2" cellspacing="0" id='submissionContainer' name = 'submissionContainer' style="color:#0196e3";>


    <tr>
      <th > Title * </th>
      <td><input name="title" size="50" type="text" class="textfield" id="title" /></td>
    </tr>

  <tr>
      <th>Event Type (*)</th>
      <td>
	  <select name="category" id="drop">
<option style="background-color:#58B9EB"; value="choose_category">Choose a category</option>
<option style="background-color:#58B9EB"; value="student_club">student club</option>
<option style="background-color:#58B9EB"; value="workshop">workshop</option>
<option style="background-color:#58B9EB"; value="conference">conference</option>
<option style="background-color:#58B9EB"; value="religious">religious</option>
<option style="background-color:#58B9EB"; value="sports">sports</option>
<option style="background-color:#58B9EB"; value="movie">movie</option>
<option style="background-color:#58B9EB"; value="show">live show</option>
</select>
	  
	  </td>
    </tr>

  <tr>
      <th> Open to: (*)</th>
      <td>
	  <select name="open" id="drop" onclick = "addAudience(this.form)">
<option style="background-color:#58B9EB"; value="all"> Entire <?php echo $_SESSION['SESS_college']; ?> </option>
<option style="background-color:#58B9EB"; value="school_row">Select School(s)</option>
<option style="background-color:#58B9EB"; value="department_row">Select Department(s)</option>
</select>
	  
	  </td>
    </tr>

<tr id = "school_row" style="display:none">
 <th></th>
<td>
<select name="school" id="drop">
<option style="background-color:#58B9EB"; value="choose">Choose School(s)</option>
<?php include('school_query.php'); ?>
</select>
</td>

<td>
<input type = "button" value = "Add" onclick="addSchool(this.form)" />
</td>
</tr>

<tr id = "department_row" style="display:none">
 <th></th>
<td>
<select name="department" id="drop">
<option style="background-color:#58B9EB"; value="choose">Choose Department(s)</option>
<?php include('department_query.php'); ?>
</select>
</td>

<td>
<input type = "button" value = "Add" onclick="addDepartment(this.form)" />
</td>
</tr>

<tr id = "dates">
<th  >Date *</th>

<td>
<table>
<tr>
<td>
	<select name="month" id="drop">
<option style="background-color:#58B9EB"; value="choose_month">month</option>
<option style="background-color:#58B9EB"; value="January">January</option>
<option style="background-color:#58B9EB"; value="February">February</option>
<option style="background-color:#58B9EB"; value="March">March</option>
<option style="background-color:#58B9EB"; value="April">April</option>
<option style="background-color:#58B9EB"; value="May">May</option>
<option style="background-color:#58B9EB"; value="June">June</option>
<option style="background-color:#58B9EB"; value="July">July</option>
<option style="background-color:#58B9EB"; value="August">August</option>
<option style="background-color:#58B9EB"; value="September">September</option>
<option style="background-color:#58B9EB"; value="October">October</option>
<option style="background-color:#58B9EB"; value="November">November</option>
<option style="background-color:#58B9EB"; value="December">December</option>

</select>
	  
	  </td>

      <td >
	<select name="day" id="drop">
<option style="background-color:#58B9EB"; value="choose_day">day</option>
<option style="background-color:#58B9EB"; value="1">1</option>
<option style="background-color:#58B9EB"; value="2">2</option>
<option style="background-color:#58B9EB"; value="3">3</option>
<option style="background-color:#58B9EB"; value="4">4</option>
<option style="background-color:#58B9EB"; value="5">5</option>
<option style="background-color:#58B9EB"; value="6">6</option>
<option style="background-color:#58B9EB"; value="7">7</option>
<option style="background-color:#58B9EB"; value="8">8</option>
<option style="background-color:#58B9EB"; value="9">9</option>
<option style="background-color:#58B9EB"; value="10">10</option>
<option style="background-color:#58B9EB"; value="11">11</option>
<option style="background-color:#58B9EB"; value="12">12</option>
<option style="background-color:#58B9EB"; value="13">13</option>
<option style="background-color:#58B9EB"; value="14">14</option>
<option style="background-color:#58B9EB"; value="15">15</option>
<option style="background-color:#58B9EB"; value="16">16</option>
<option style="background-color:#58B9EB"; value="17">17</option>
<option style="background-color:#58B9EB"; value="18">18</option>
<option style="background-color:#58B9EB"; value="19">19</option>
<option style="background-color:#58B9EB"; value="20">20</option>
<option style="background-color:#58B9EB"; value="21">21</option>
<option style="background-color:#58B9EB"; value="22">22</option>
<option style="background-color:#58B9EB"; value="23">23</option>
<option style="background-color:#58B9EB"; value="24">24</option>
<option style="background-color:#58B9EB"; value="25">25</option>
<option style="background-color:#58B9EB"; value="26">26</option>
<option style="background-color:#58B9EB"; value="27">27</option>
<option style="background-color:#58B9EB"; value="28">28</option>
<option style="background-color:#58B9EB"; value="29">29</option>
<option style="background-color:#58B9EB"; value="30">30</option>
<option style="background-color:#58B9EB"; value="31">31</option>
</select>
	  
	  </td>
	 

	 
      <td >
	<select name="year" id="drop">
<option style="background-color:#58B9EB"; value="choose_year">year</option>
<option style="background-color:#58B9EB"; value="2012">2012</option>
<option style="background-color:#58B9EB"; value="2013">2013</option>
<option style="background-color:#58B9EB"; value="2014">2014</option>
<option style="background-color:#58B9EB"; value="2015">2015</option>
<option style="background-color:#58B9EB"; value="2016">2016</option>
</select>
	  
	  </td>
</tr>
</table>
</td>
	 
    </tr>

<tr >
<th  >Start time </th>

     <td>

<table>
<tr>
<td>

	<select name="start_hour" id="drop" align="left">
<option style="background-color:#58B9EB"; value="1">1</option>
<option style="background-color:#58B9EB"; value="2">2</option>
<option style="background-color:#58B9EB"; value="3">3</option>
<option style="background-color:#58B9EB"; value="4">4</option>
<option style="background-color:#58B9EB"; value="5">5</option>
<option style="background-color:#58B9EB"; value="6">6</option>
<option style="background-color:#58B9EB"; value="7">7</option>
<option style="background-color:#58B9EB"; value="8">8</option>
<option style="background-color:#58B9EB"; value="9">9</option>
<option style="background-color:#58B9EB"; value="10">10</option>
<option style="background-color:#58B9EB"; value="11">11</option>
<option style="background-color:#58B9EB"; value="12">12</option>
</select>
	  
	  </td>

   <td >
	<select name="start_minute" id="drop" align="left">
<option style="background-color:#58B9EB"; value="00">00</option>
<option style="background-color:#58B9EB"; value="01">01</option>
<option style="background-color:#58B9EB"; value="02">02</option>
<option style="background-color:#58B9EB"; value="03">03</option>
<option style="background-color:#58B9EB"; value="04">04</option>
<option style="background-color:#58B9EB"; value="05">05</option>
<option style="background-color:#58B9EB"; value="06">06</option>
<option style="background-color:#58B9EB"; value="07">07</option>
<option style="background-color:#58B9EB"; value="08">08</option>
<option style="background-color:#58B9EB"; value="09">09</option>
<option style="background-color:#58B9EB"; value="10">10</option>
<option style="background-color:#58B9EB"; value="11">11</option>
<option style="background-color:#58B9EB"; value="12">12</option>

<option style="background-color:#58B9EB"; value="13">13</option>
<option style="background-color:#58B9EB"; value="14">14</option>
<option style="background-color:#58B9EB"; value="15">15</option>
<option style="background-color:#58B9EB"; value="16">16</option>
<option style="background-color:#58B9EB"; value="17">17</option>
<option style="background-color:#58B9EB"; value="18">18</option>
<option style="background-color:#58B9EB"; value="19">19</option>
<option style="background-color:#58B9EB"; value="20">20</option>
<option style="background-color:#58B9EB"; value="21">21</option>
<option style="background-color:#58B9EB"; value="22">22</option>

<option style="background-color:#58B9EB"; value="23">23</option>
<option style="background-color:#58B9EB"; value="24">24</option>
<option style="background-color:#58B9EB"; value="25">25</option>
<option style="background-color:#58B9EB"; value="26">26</option>
<option style="background-color:#58B9EB"; value="27">27</option>
<option style="background-color:#58B9EB"; value="28">28</option>
<option style="background-color:#58B9EB"; value="29">29</option>
<option style="background-color:#58B9EB"; value="30">30</option>
<option style="background-color:#58B9EB"; value="31">31</option>
<option style="background-color:#58B9EB"; value="32">32</option>

<option style="background-color:#58B9EB"; value="33">33</option>
<option style="background-color:#58B9EB"; value="34">34</option>
<option style="background-color:#58B9EB"; value="35">35</option>
<option style="background-color:#58B9EB"; value="36">36</option>
<option style="background-color:#58B9EB"; value="37">37</option>
<option style="background-color:#58B9EB"; value="38">38</option>
<option style="background-color:#58B9EB"; value="39">39</option>
<option style="background-color:#58B9EB"; value="40">40</option>
<option style="background-color:#58B9EB"; value="41">41</option>
<option style="background-color:#58B9EB"; value="42">42</option>


<option style="background-color:#58B9EB"; value="43">43</option>
<option style="background-color:#58B9EB"; value="44">44</option>
<option style="background-color:#58B9EB"; value="45">45</option>
<option style="background-color:#58B9EB"; value="46">46</option>
<option style="background-color:#58B9EB"; value="47">47</option>
<option style="background-color:#58B9EB"; value="48">48</option>
<option style="background-color:#58B9EB"; value="49">49</option>
<option style="background-color:#58B9EB"; value="50">50</option>
<option style="background-color:#58B9EB"; value="51">51</option>
<option style="background-color:#58B9EB"; value="52">52</option>

<option style="background-color:#58B9EB"; value="53">53</option>
<option style="background-color:#58B9EB"; value="54">54</option>
<option style="background-color:#58B9EB"; value="55">55</option>
<option style="background-color:#58B9EB"; value="56">56</option>
<option style="background-color:#58B9EB"; value="57">57</option>
<option style="background-color:#58B9EB"; value="58">58</option>
<option style="background-color:#58B9EB"; value="59">59</option>


</select>
	  
	  </td>


<td >    
<select name="start" id="drop" >
<option style="background-color:#58B9EB"; value="AM">AM</option>
<option style="background-color:#58B9EB"; value="PM">PM</option>
</select>
	  </td>


</tr>
</table>
</td>

</tr>

<tr>

<th  >End time </th>
 <td>

<table>
<tr>
<td>

	<select name="end_hour" id="drop" >
<option style="background-color:#58B9EB"; value="1">1</option>
<option style="background-color:#58B9EB"; value="2">2</option>
<option style="background-color:#58B9EB"; value="3">3</option>
<option style="background-color:#58B9EB"; value="4">4</option>
<option style="background-color:#58B9EB"; value="5">5</option>
<option style="background-color:#58B9EB"; value="6">6</option>
<option style="background-color:#58B9EB"; value="7">7</option>
<option style="background-color:#58B9EB"; value="8">8</option>
<option style="background-color:#58B9EB"; value="9">9</option>
<option style="background-color:#58B9EB"; value="10">10</option>
<option style="background-color:#58B9EB"; value="11">11</option>
<option style="background-color:#58B9EB"; value="12">12</option>
</select>
	  
	  </td>

   <td >
	<select name="end_minute" id="drop">
<option style="background-color:#58B9EB"; value="00">00</option>
<option style="background-color:#58B9EB"; value="01">01</option>
<option style="background-color:#58B9EB"; value="02">02</option>
<option style="background-color:#58B9EB"; value="03">03</option>
<option style="background-color:#58B9EB"; value="04">04</option>
<option style="background-color:#58B9EB"; value="05">05</option>
<option style="background-color:#58B9EB"; value="06">06</option>
<option style="background-color:#58B9EB"; value="07">07</option>
<option style="background-color:#58B9EB"; value="08">08</option>
<option style="background-color:#58B9EB"; value="09">09</option>
<option style="background-color:#58B9EB"; value="10">10</option>
<option style="background-color:#58B9EB"; value="11">11</option>
<option style="background-color:#58B9EB"; value="12">12</option>

<option style="background-color:#58B9EB"; value="13">13</option>
<option style="background-color:#58B9EB"; value="14">14</option>
<option style="background-color:#58B9EB"; value="15">15</option>
<option style="background-color:#58B9EB"; value="16">16</option>
<option style="background-color:#58B9EB"; value="17">17</option>
<option style="background-color:#58B9EB"; value="18">18</option>
<option style="background-color:#58B9EB"; value="19">19</option>
<option style="background-color:#58B9EB"; value="20">20</option>
<option style="background-color:#58B9EB"; value="21">21</option>
<option style="background-color:#58B9EB"; value="22">22</option>

<option style="background-color:#58B9EB"; value="23">23</option>
<option style="background-color:#58B9EB"; value="24">24</option>
<option style="background-color:#58B9EB"; value="25">25</option>
<option style="background-color:#58B9EB"; value="26">26</option>
<option style="background-color:#58B9EB"; value="27">27</option>
<option style="background-color:#58B9EB"; value="28">28</option>
<option style="background-color:#58B9EB"; value="29">29</option>
<option style="background-color:#58B9EB"; value="30">30</option>
<option style="background-color:#58B9EB"; value="31">31</option>
<option style="background-color:#58B9EB"; value="32">32</option>

<option style="background-color:#58B9EB"; value="33">33</option>
<option style="background-color:#58B9EB"; value="34">34</option>
<option style="background-color:#58B9EB"; value="35">35</option>
<option style="background-color:#58B9EB"; value="36">36</option>
<option style="background-color:#58B9EB"; value="37">37</option>
<option style="background-color:#58B9EB"; value="38">38</option>
<option style="background-color:#58B9EB"; value="39">39</option>
<option style="background-color:#58B9EB"; value="40">40</option>
<option style="background-color:#58B9EB"; value="41">41</option>
<option style="background-color:#58B9EB"; value="42">42</option>


<option style="background-color:#58B9EB"; value="43">43</option>
<option style="background-color:#58B9EB"; value="44">44</option>
<option style="background-color:#58B9EB"; value="45">45</option>
<option style="background-color:#58B9EB"; value="46">46</option>
<option style="background-color:#58B9EB"; value="47">47</option>
<option style="background-color:#58B9EB"; value="48">48</option>
<option style="background-color:#58B9EB"; value="49">49</option>
<option style="background-color:#58B9EB"; value="50">50</option>
<option style="background-color:#58B9EB"; value="51">51</option>
<option style="background-color:#58B9EB"; value="52">52</option>

<option style="background-color:#58B9EB"; value="53">53</option>
<option style="background-color:#58B9EB"; value="54">54</option>
<option style="background-color:#58B9EB"; value="55">55</option>
<option style="background-color:#58B9EB"; value="56">56</option>
<option style="background-color:#58B9EB"; value="57">57</option>
<option style="background-color:#58B9EB"; value="58">58</option>
<option style="background-color:#58B9EB"; value="59">59</option>


</select>
	  
	  </td>


<td>    
<select name="end" id="drop">
<option style="background-color:#58B9EB"; value="AM">AM</option>
<option style="background-color:#58B9EB"; value="PM">PM</option>
</select>
	  </td>


</tr>
</table>
</td>


    </tr>





	
	   <tr>
      <th  >Location *</th>
      <td><input name="location" size="50" type="text" class="textfield" id="location" /></td>
    </tr>
	
	 

	   <tr>
      <th >Admission *</th>
      <td>
	  <select name="admission" id="drop"  onclick="addAmount2(this.form)">
<option style="background-color:#58B9EB"; value="0">free</option>
<option style="background-color:#58B9EB"; value="1">paid</option>
</select>
	  
	  </td>
    </tr>



   <tr id="myDiv">
      

    </tr>

	   <tr>
      <th>Reservation required? *</th>
      <td >
	  <select name="reservation" id="drop">
<option style="background-color:#58B9EB"; value="0">no</option>
<option style="background-color:#58B9EB"; value="1">yes</option>
</select>
	  
	  </td>
    </tr>

<tr>

<th>  </th> 
<td>
<input type="checkbox" name="contact_check" id="contact_check" value="1" onclick="FillContactDetails(this.form)"> use my contact details
</td>
</tr>

    <tr>
      <th> Contact Person </th>
      <td><input name="person" size="50" type="text" class="textfield" id="person"  /></td>
    </tr>

    <tr>
      <th> Contact Email  </th>
      <td><input name="contact_email" size="50" type="text" class="textfield" id="contact_email"  /></td>
    </tr>
    <tr>
      <th>Contact Phone </th>
      <td><input name="phone" type="text" class="textfield" id="phone"  /></td>
    </tr>

    <tr>
      <th> Event Website </th>
      <td><input name="website" size="50" type="text" class="textfield" id="website" /></td>
    </tr> 


<tr>
<th>  </th> 
<td>
 
Check all that applies:
</td>
</tr>
  

<tr>
<th>  </th> 
<td>
 
<input type="checkbox" name="refreshments" id="refreshments" value="1"> Refreshments served
<input type="checkbox" name="raffle" id="raffle" value="1"> Raffle
</td>
</tr>




<tr>

<th width="40%">
    Event Details *
</th>


<td>


<textarea name="details" id="details" style="width:400px; height:200px;" onfocus="this.value=''; setbg('#e5fff3');" onblur="setbg('white')"></textarea>

</td>
</tr>



<tr>
<th>
</th>
<td>
		<input type="submit" name='submit' id="submit" value="Submit" >
</td>
</tr> 



</table>


</form>

<?php
}

else
{
echo '<div id="submissionPromotion" style="color:#0196e3";>
<h2> Only members can add events.  <a href="login.php"> Login </a> or <a href="register.php"> Signup </a> </h2>
</div>';
}
?>


  </div>
  <div class="span2">
  <!--adverts-->
<?php
include('adverts.php');
?>

  </div>
</div>
</div>

  </body>
</html>
