<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	
	//Sanitize the POST values
	$school = clean($_POST['school']);
	$department = clean($_POST['department']);
	
	//Input Validations
	if($school == 'choose school') {
		$errmsg_arr[] = 'School missing';
		$errflag = true;
	}

	//Input Validations
	if($department == '') {
		$errmsg_arr[] = 'Department missing';
		$errflag = true;
	}


	//If there are input validations, redirect back to the  form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: add_department.php");
		exit();
	}
	
	//Create INSERT query
	$qry = "INSERT INTO departments(college, school, department) 
	VALUES('".$_SESSION['SESS_college']."','$school', '$department')";
	$result = @mysql_query($qry);
	
	//Check whether the query was successful or not
	if($result) {
		header("location: events2.php");
		exit();
	}else {
		die("Query failed");
	}
?>
