<?php
	//Start session
	session_start();
	
	//Include database connection details
	//require_once('connect.php');
	include("connect.php");
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

//email format
	function isValidEmail($email){
	return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

//name format

function isValidName($name){
return eregi ("^[a-zA-Z][a-zA-Z -]*$", $name);
}


	
//Sanitize the POST values
$email_address = clean($_POST['email_address']);
$email_address2 = clean($_POST['email_address2']);
$status = clean($_POST['status']);


if($status == 'choose status') {
	$errmsg_arr[] = 'Status missing';
	$errflag = true;
}
	
if($email_address == '') {
	$errmsg_arr[] = 'Email missing';
	$errflag = true;
}
else
{
if(isValidEmail($email_address) ==TRUE)
{
}
else
{
$errmsg_arr[] = 'Email address is not valid';
		$errflag = true;
}
}
	


	
//Check for duplicate login ID
if($email_address != '') {
	$qry = "SELECT * FROM users WHERE email_address='$email_address'";
	$result = mysql_query($qry);
	if($result) {
		if(mysql_num_rows($result) == 0) {
			$errmsg_arr = array();
			$errmsg_arr[] = 'Email Address is not registered on Campus Pages. ';
			$errflag = true;
		}
		@mysql_free_result($result);
	}
	else {
		die("Query failed");
	}
}

//If there are input validations, redirect back to the registration form
if($errflag) {
	$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
	session_write_close();
	header("location: make_admin.php");
	exit();
}


	//Create INSERT query
$qry= "INSERT INTO admin(email_address, status) VALUES ('$email_address', '$status')";

$result = @mysql_query($qry);

//Check whether the query was successful or not
if($result) {

	header("location: events2.php");
	exit();
}else {
	die("Query failed");
}
?>
