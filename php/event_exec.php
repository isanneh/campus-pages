<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');

	//Include database connection details
	include('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	/*//Connect to mysql server
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	if(!$link) {
		die('Failed to connect to server: ' . mysql_error());
	}


	//Select database
	$db = mysql_select_db(DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}
*/

	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	
	//Sanitize the POST values
	
$title = clean($_POST['title']);
$category = clean($_POST['category']);
$details = clean($_POST['details']);
$month = clean($_POST['month']);
$day = clean($_POST['day']);
$year = clean($_POST['year']);
$start_hour = clean($_POST['start_hour']);
$start_minute = clean($_POST['start_minute']);
$start = clean($_POST['start']);
$end_hour = clean($_POST['end_hour']);
$end_minute = clean($_POST['end_minute']);
$end = clean($_POST['end']);
$location = clean($_POST['location']);
$admission = clean($_POST['admission']);
$reservation = clean($_POST['reservation']);
$amount = clean($_POST['amount']);
$person = clean($_POST['person']);
$contact_email = clean($_POST['contact_email']);
$phone = clean($_POST['phone']);

$website = clean($_POST['website']);
$refreshments = clean($_POST['refreshments']);
$raffle = clean($_POST['raffle']);

$open = clean($_POST['open']);
$school_row = $_POST['school_row'];
$department_row = $_POST['department_row'];
$school_list = $_POST['school_list'];
$department_list = $_POST['department_list'];


	
	//Input Validations


if($category == 'choose_category') {
		$errmsg_arr[] = 'No category chosen!';
		$errflag = true;
	}

if($title == '') {
		$errmsg_arr[] = 'No title added!';
		$errflag = true;
	}

if($details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}

if($month == 'choose_month') {
		$errmsg_arr[] = 'No month chosen!';
		$errflag = true;
	}

if($day == 'choose_day') {
		$errmsg_arr[] = 'No day chosen!';
		$errflag = true;
	}

if($year == 'choose_year') {
		$errmsg_arr[] = 'No year chosen!';
		$errflag = true;
	}

if($location == '') {
		$errmsg_arr[] = 'No location chosen!';
		$errflag = true;
	}

if($open == 'department_row') {
	if($department_row == 'all') {
		$errmsg_arr[] = 'No department chosen!';
		$errflag = true;
	}

	}
else if($open == 'school_row') {
	if($school_row == 'all') {
		$errmsg_arr[] = 'No school chosen!';
		$errflag = true;
	}

	}



/*
if($amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}
*/

/*if($person == '') {
		//$errmsg_arr[] = 'No contact person added!';
		//$errflag = true;
		$person='NULL'
	}
*/
/*if($email == '') {
		$errmsg_arr[] = 'Submitter email not provided!';
		$errflag = true;
	}
*/

/*if($website == '') {
		$website='NULL';
	}

if($phone == '') {
		$phone='NULL';
	}
*/



if (isset($_POST['refreshments']))
	{
		$refreshments=1;
	}
else
	{
		$refreshments=0;
	}

if (isset($_POST['raffle']))
	{
		$raffle=1;
	}
else
	{
		$raffle=0;
	}

	

	
	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: add_events2.php");
		exit();
	}

	if($open == "all")    {

	    $audience = 0;
	}

	else {
	    $audience = 1;
	    }

	//Create query
	$qry = "INSERT INTO `events`(`college`, `title`, `details`, `start_hour`, `start_minute`, `start`, 
`end_hour`, `end_minute`, `end`, `month`, `day`, `year`,`category`,   `location`, `submitter_email`, `contact_email`, `website`, `phone`, `name`,  `admission`, `amount`, `reservation`, `refreshments`, `raffle`, `status`, `audience`) 
VALUES ('".$_SESSION['SESS_college']."', '$title',  '$details',   '$start_hour', '$start_minute', '$start', '$end_hour', '$end_minute', '$end', '$month','$day','$year','$category', '$location',   '".$_SESSION['SESS_EMAILADDRESS']."', '$contact_email',  '$website', '$phone', '$person','$admission', '$amount', '$reservation', '$refreshments', '$raffle', 'pending', '$audience')"; 

	$result=@mysql_query($qry);
	if($result)
	    {

	    }
	else
	    {
		die("query failed32");
	    }

	if($open == "school_row")    {
	//$id = $link->lastInsertId();
 	mysql_query("SET @id = LAST_INSERT_ID()"); 
	//$id = 1;



	foreach ($school_list as $school)
	    {
		//Create query
		$qry_school = "INSERT INTO `audience_school`(`event_id`, `school`) 
	VALUES (@id, '$school')"; 

		$result_school =@mysql_query($qry_school);
		if($result_school)
		    {

		    }
		else
		    {
			die("query failed32");
		    }
	    }
	}

	else if($open == "department_row")    {
	//$id = $link->lastInsertId();
 	mysql_query("SET @id = LAST_INSERT_ID()"); 
	//$id = 1;



	foreach ($department_list as $department)
	    {
		//Create query
		$qry_department = "INSERT INTO `audience_department`(`event_id`, `department`) 
	VALUES (@id, '$department')"; 

		$result_department =@mysql_query($qry_department);
		if($result_department)
		    {

		    }
		else
		    {
			die("query failed32");
		    }
	    }
	}

	
	header("location: events2.php");
	exit();

			

?>
