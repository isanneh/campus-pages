<?php

	/*This file updates the status of the classified to:
	(i) approved => this approves the listing
	(ii) disapproved => this rejects the listing
	(iii) review => this indicates that the listing has been reviewed, and further information is needed from submitter
	*/

	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}


	
//Sanitize the POST values


$status = clean($_POST['status']);
$prev_status = clean($_POST['prev_status']);
$id = clean($_POST['id']);
$category = clean($_POST['category']);




	//Input Validations


if($status == 'choose status') {
		$errmsg_arr[] = 'No status chosen!';
		$errflag = true;
	}
	
	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		$header_url = "location:";
		$header_url .= $url;
		header($header_url);
		exit();
	}

if($status == $prev_status) {
		$msg='This classified has already been ';
		$msg .= $prev_status;
		$errmsg_arr[] = $msg;
		$errflag = true;
	}

if($category == 'for sale')
{

$page="forsale-page.php";	

}

else if($category == 'housing wanted')
{
$page="housing-wanted-page.php";	

}

else if($category == 'housing available')
{

$page="housing-available-page.php";	
}

else if($category == 'job')
{
$page="jobs-page.php";	
}

else if($category == 'internship')
{
$page="internships-page.php";	
}

else if($category == 'reu')
{
$page="reu-page.php";	
}

else if($category == 'services')
{
$page="services-page.php";	
}

else if($category == 'tutoring')
{
$page="tutoring-page.php";	

}

else if($category == 'textbooks wanted')
{
$page="textbooks-wanted-page.php";	
}

else if($category == 'textbooks for sale')
{
$page="textbooks-forsale-page.php";	

}


$url =$page;
$url .="?id=";
$url .=$id;



$qry_log="INSERT into log (category, approved_by, url, status) VALUES ('$category', '".$_SESSION['SESS_EMAILADDRESS']."', '$url', '$status')";
$result_log=mysql_query($qry_log);
if(! $result_log)
{
 die ("log query failed!");
}

if($category == 'for sale')
{

	
//Create query
$qry="UPDATE `for_sale` SET `status`='$status' WHERE id='$id'";




	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}
}

else if($category == 'housing wanted')
{

	
//Create query
$qry="UPDATE `housing_wanted` SET `status`='$status' WHERE id='$id'";

	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'housing available')
{

	
//Create query

$qry="UPDATE `housing_available` SET `status`='$status' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'job')
{

	
//Create query

$qry="UPDATE `job` SET `status`='$status' WHERE id='$id'";

	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'internship')
{

	
//Create query

$qry="UPDATE `internship` SET `status`='$status' WHERE id='$id'";

	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'reu')
{

//Create query

$qry="UPDATE `reu` SET `status`='$status' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'services')
{

	
//Create query


$qry="UPDATE `services` SET `status`='$status' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'tutoring')
{

	
//Create query

$qry="UPDATE `tutoring` SET `status`='$status' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'textbooks wanted')
{

	
//Create query

$qry="UPDATE `textbooks_wanted` SET `status`='$status' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'textbooks for sale')
{

	
//Create query

$qry="UPDATE `textbooks_for_sale` SET `status`='$status' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{
	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}






			

?>
