<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	


	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	
	//Sanitize the POST values
	$email_address = clean($_POST['email_address']);
	$password = clean($_POST['password']);

	
	//Input Validations
	if($email_address == '') {
		$errmsg_arr[] = 'Email Address missing';
		$errflag = true;
	}
	if($password == '') {
		$errmsg_arr[] = 'Password missing';
		$errflag = true;
	}
	
	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: login.php");
		exit();
	}
	
	//Create query
	$qry="SELECT * FROM users WHERE `email_address`='$email_address' AND `password`='".md5($_POST['password'])."'";
	$result=mysql_query($qry);

	//Create query
	$qry2="SELECT * FROM admin WHERE `email_address`='$email_address'";
	$result2=mysql_query($qry2);

	//Check whether the query was successful or not
	if($result && $result2) {
		if(mysql_num_rows($result) == 1) {
			//Login Successful
			//session_regenerate_id();
			$member = mysql_fetch_assoc($result);
			$admin = mysql_fetch_assoc($result2);
			$_SESSION['SESS_FIRSTNAME'] = $member['first_name'];
			$_SESSION['SESS_LASTNAME'] = $member['last_name'];
			$_SESSION['SESS_SCHOOL'] = $member['school'];
			$_SESSION['SESS_EMAILADDRESS'] = $member['email_address'];
			$_SESSION['SESS_PHONE'] = $member['phone'];

			if(mysql_num_rows($result2) == 1) {
				if($admin['status'] == 'admin') {
					$_SESSION['SESS_STATUS'] = 'admin';
								   }
				else if($admin['status'] == 'superadmin') {
					$_SESSION['SESS_STATUS'] = 'superadmin';
								   	}
								}
			else
			{
				$_SESSION['SESS_STATUS'] = 'member';
			}

			//session_write_close();
			header("location: events2.php");
			exit();
		}
		else 
		{
			//Login failed
			echo "Login failed!";
			//header("location: login-failed.php");
			//exit();
		}
	}
else {
		die("Query failed");
	}

?>
