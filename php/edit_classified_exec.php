<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	/*//Connect to mysql server
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
	if(!$link) {
		die('Failed to connect to server: ' . mysql_error());
	}


	//Select database
	$db = mysql_select_db(DB_DATABASE);
	if(!$db) {
		die("Unable to select database");
	}
*/

	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}


	
//Sanitize the POST values

$id = clean($_POST['id']);
	
$title = clean($_POST['title']);
$category = clean($_POST['category']);
$sale_details = clean($_POST['sale_details']);
$sale_amount = clean($_POST['sale_amount']);
$sale_person = clean($_POST['sale_person']);
$sale_contact_email = clean($_POST['sale_contact_email']);
$sale_phone = clean($_POST['sale_phone']);
$sale_check_email = clean($_POST['sale_check_email']);
$sale_check_phone = clean($_POST['sale_check_phone']);


$housing_wanted_details = clean($_POST['housing_wanted_details']);
$housing_wanted_amount = clean($_POST['housing_wanted_amount']);
$housing_wanted_person = clean($_POST['housing_wanted_person']);
$housing_wanted_contact_email = clean($_POST['housing_wanted_contact_email']);
$housing_wanted_phone = clean($_POST['housing_wanted_phone']);
$housing_wanted_check_email = clean($_POST['housing_wanted_check_email']);
$housing_wanted_check_phone = clean($_POST['housing_wanted_check_phone']);
$housing_wanted_duration = clean($_POST['housing_wanted_duration']);

$housing_available_details = clean($_POST['housing_available_details']);
$housing_available_amount = clean($_POST['housing_available_amount']);
$housing_available_person = clean($_POST['housing_available_person']);
$housing_available_contact_email = clean($_POST['housing_available_contact_email']);
$housing_available_phone = clean($_POST['housing_available_phone']);
$housing_available_check_email = clean($_POST['housing_available_check_email']);
$housing_available_check_phone = clean($_POST['housing_available_check_phone']);
$housing_available_duration = clean($_POST['housing_available_duration']);

$job_details = clean($_POST['job_details']);
$job_amount = clean($_POST['job_amount']);
$job_person = clean($_POST['job_person']);
$job_contact_email = clean($_POST['job_contact_email']);
$job_phone = clean($_POST['job_phone']);
$job_check_email = clean($_POST['job_check_email']);
$job_check_phone = clean($_POST['job_check_phone']);
$job_website = clean($_POST['job_website']);

$internship_details = clean($_POST['internship_details']);
$internship_amount = clean($_POST['internship_amount']);
$internship_person = clean($_POST['internship_person']);
$internship_contact_email = clean($_POST['internship_contact_email']);
$internship_phone = clean($_POST['internship_phone']);
$internship_check_email = clean($_POST['internship_check_email']);
$internship_check_phone = clean($_POST['internship_check_phone']);
$internship_duration = clean($_POST['internship_duration']);
$internship_month = clean($_POST['internship_month']);
$internship_day = clean($_POST['internship_day']);
$internship_year = clean($_POST['internship_year']);
$internship_check_deadline = clean($_POST['internship_check_deadline']);


$reu_details = clean($_POST['reu_details']);
$reu_amount = clean($_POST['reu_amount']);
$reu_person = clean($_POST['reu_person']);
$reu_contact_email = clean($_POST['reu_contact_email']);
$reu_phone = clean($_POST['reu_phone']);
$reu_check_email = clean($_POST['reu_check_email']);
$reu_check_phone = clean($_POST['reu_check_phone']);
$reu_duration = clean($_POST['reu_duration']);
$reu_month = clean($_POST['reu_month']);
$reu_day = clean($_POST['reu_day']);
$reu_year = clean($_POST['reu_year']);
$reu_check_deadline = clean($_POST['reu_check_deadline']);

$services_details = clean($_POST['services_details']);
$services_amount = clean($_POST['services_amount']);
$services_person = clean($_POST['services_person']);
$services_contact_email = clean($_POST['services_contact_email']);
$services_phone = clean($_POST['services_phone']);
$services_check_email = clean($_POST['services_check_email']);
$services_check_phone = clean($_POST['services_check_phone']);
$services_website = clean($_POST['services_website']);

$tutoring_details = clean($_POST['tutoring_details']);
$tutoring_amount = clean($_POST['tutoring_amount']);
$tutoring_person = clean($_POST['tutoring_person']);
$tutoring_contact_email = clean($_POST['tutoring_contact_email']);
$tutoring_phone = clean($_POST['tutoring_phone']);
$tutoring_check_email = clean($_POST['tutoring_check_email']);
$tutoring_check_phone = clean($_POST['tutoring_check_phone']);

$textbooks_wanted_details = clean($_POST['textbooks_wanted_details']);
$textbooks_wanted_amount = clean($_POST['textbooks_wanted_amount']);
$textbooks_wanted_person = clean($_POST['textbooks_wanted_person']);
$textbooks_wanted_contact_email = clean($_POST['textbooks_wanted_contact_email']);
$textbooks_wanted_phone = clean($_POST['textbooks_wanted_phone']);
$textbooks_wanted_check_email = clean($_POST['textbooks_wanted_check_email']);
$textbooks_wanted_check_phone = clean($_POST['textbooks_wanted_check_phone']);

$textbooks_for_sale_details = clean($_POST['textbooks_for_sale_details']);
$textbooks_for_sale_amount = clean($_POST['textbooks_for_sale_amount']);
$textbooks_for_sale_person = clean($_POST['textbooks_for_sale_person']);
$textbooks_for_sale_contact_email = clean($_POST['textbooks_for_sale_contact_email']);
$textbooks_for_sale_phone = clean($_POST['textbooks_for_sale_phone']);
$textbooks_for_sale_check_email = clean($_POST['textbooks_for_sale_check_email']);
$textbooks_for_sale_check_phone = clean($_POST['textbooks_for_sale_check_phone']);





	
	//Input Validations


if($category == 'choose_category') {
		$errmsg_arr[] = 'No category chosen!';
		$errflag = true;
	}


if($title == '') {
		$errmsg_arr[] = 'No title added!';
		$errflag = true;
	}

if($category == 'for sale') {

if($sale_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($sale_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}



if (isset($_POST['sale_check_phone']))
	{
		$sale_check_phone=1;
	}
else
	{
		$sale_check_phone=0;
	}

if (isset($_POST['sale_check_email']))
	{
		$sale_check_email=1;
	}
else
	{
		$sale_check_email=0;
	}

}



else if($category == 'housing wanted') {

if($housing_wanted_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($housing_wanted_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


if (isset($_POST['housing_wantedcheck_phone']))
	{
		$housing_wanted_check_phone=1;
	}
else
	{
		$housing_wanted_check_phone=0;
	}

if (isset($_POST['housing_wantedcheck_email']))
	{
		$housing_wanted_check_email=1;
	}
else
	{
		$housing_wanted_check_email=0;


	}
}

else if($category == 'housing available') {

if($housing_available_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($housing_available_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}



if (isset($_POST['housing_available_check_phone']))
	{
		$housing_available_check_phone=1;
	}
else
	{
		$housing_available_check_phone=0;
	}

if (isset($_POST['housing_available_check_email']))
	{
		$housing_available_check_email=1;
	}
else
	{
		$housing_available_check_email=0;
	}


}

else if($category == 'job') {

if($job_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if (isset($_POST['job_check_phone']))
	{
		$job_check_phone=1;
	}
else
	{
		$job_check_phone=0;
	}

if (isset($_POST['job_check_email']))
	{
		$job_check_email=1;
	}
else
	{
		$job_check_email=0;
	}

}

else if($category == 'internship') {

if($internship_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}



if (isset($_POST['internship_check_phone']))
	{
		$internship_check_phone=1;
	}
else
	{
		$internship_check_phone=0;
	}

if (isset($_POST['internship_check_deadline']))
	{
	$internship_month="";
	$internship_day="";
	$internship_year="";
	}
else
	{
	$num=0;
	if($internship_month == '') {
			$errmsg_arr[] = 'No month added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($internship_day == '') {
			$errmsg_arr[] = 'No day added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($internship_year == '') {
			$errmsg_arr[] = 'No year added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($num != 0)
		{
			$errmsg_arr[] = 'No deadline added!';
			$errflag = true;
		}
	}

}


else if($category == 'reu') {

if($reu_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if (isset($_POST['reu_check_phone']))
	{
		$reu_check_phone=1;
	}
else
	{
		$reu_check_phone=0;
	}

if (isset($_POST['reu_check_deadline']))
	{
	$reu_month="";
	$reu_day="";
	$reu_year="";
	}
else
	{
	$num=0;
	if($reu_month == '') {
			$errmsg_arr[] = 'No month added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($reu_day == '') {
			$errmsg_arr[] = 'No day added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($reu_year == '') {
			$errmsg_arr[] = 'No year added!';
			$errflag = true;
	$num=$num + 1;
		}

	if($num != 0)
		{
			$errmsg_arr[] = 'No deadline added!';
			$errflag = true;
		}
	}

}

else if($category == 'services') {

if($services_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($services_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


if (isset($_POST['services_check_phone']))
	{
		$services_check_phone=1;
	}
else
	{
		$services_check_phone=0;
	}


}

else if($category == 'tutoring') {

if($tutoring_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($tutoring_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


if (isset($_POST['tutoring_check_phone']))
	{
		$tutoring_check_phone=1;
	}
else
	{
		$tutoring_check_phone=0;
	}


}

else if($category == 'textbooks wanted') {

if($textbooks_wanted_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($textbooks_wanted_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


if (isset($_POST['textbooks_wanted_check_phone']))
	{
		$textbooks_wanted_check_phone=1;
	}
else
	{
		$textbooks_wanted_check_phone=0;
	}


}

else if($category == 'textbooks for sale') {

if($textbooks_for_sale_details == '') {
		$errmsg_arr[] = 'No details added!';
		$errflag = true;
	}


if($textbooks_for_sale_amount == '') {
		$errmsg_arr[] = 'No amount added!';
		$errflag = true;
	}


if (isset($_POST['textbooks_for_sale_check_phone']))
	{
		$textbooks_for_sale_check_phone=1;
	}
else
	{
		$textbooks_for_sale_check_phone=0;
	}


}







	

	
	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: classified.php");
		exit();
	}

if($category == 'for sale')
{

	$qry_current_data="SELECT * FROM for_sale_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

echo mysql_num_rows($result_current_data);

			$qry_copy="INSERT INTO for_sale_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`,  `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `amount`,  `check_email`, `check_phone`, `details`, 'original version' FROM for_sale WHERE id='$id'";


			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data

	
//Create query
$qry="UPDATE `for_sale` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$sale_contact_email', `phone`='$sale_phone', `name`='$sale_person', `amount`='$sale_amount',  `details`='$sale_details' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{

		//Create query
		$qry_edit="INSERT INTO `for_sale_edits`(`id` ,`school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`,  `check_email`, `check_phone`, `details`, `info`) 
		VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$sale_contact_email', '$sale_phone', '$sale_person', '$sale_amount',  '$sale_check_email', '$sale_check_phone', '$sale_details', 'edited version')"; 


		$result_edit=@mysql_query($qry_edit);
		if(! $result_edit)
		{
		die("query failed32");
		}

		header("location: events2.php");
		exit();
	}
	else
	{
	die("query failed33");
	}

}

else if($category == 'housing wanted')
{

	$qry_current_data="SELECT * FROM housing_wanted_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO housing_wanted_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`, 'original version' FROM housing_wanted WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data

	
//Create query
$qry="UPDATE `housing_wanted` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$housing_wanted_contact_email', `phone`='$housing_wanted_phone', `name`='$housing_wanted_person', `amount`='$housing_wanted_amount', `duration`='$housing_wanted_duration', `details`='$housing_wanted_details' WHERE id='$id'";

	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `housing_wanted_edits`(`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$housing_wanted_contact_email', '$housing_wanted_phone', '$housing_wanted_person', '$housing_wanted_amount', '$housing_wanted_duration',  '$housing_wanted_check_email', '$housing_wanted_check_phone', '$housing_wanted_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}

	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'housing available')
{

	$qry_current_data="SELECT * FROM housing_available_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO housing_available_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`, 'original version' FROM housing_available WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data

	
//Create query

$qry="UPDATE `housing_available` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$housing_available_contact_email', `phone`='$housing_available_phone', `name`='$housing_available_person', `amount`='$housing_available_amount', `duration`='$housing_available_duration', `details`='$housing_available_details' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `housing_available_edits`(`id`,`school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `duration`,  `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$housing_available_contact_email', '$housing_available_phone', '$housing_available_person', '$housing_available_amount', '$housing_available_duration',  '$housing_available_check_email', '$housing_available_check_phone', '$housing_available_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}

	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'job')
{

	$qry_current_data="SELECT * FROM job_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO job_edits (`id`, `school`, `edited_`school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, `edited_`school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `website`, `check_email`, `check_phone`, `details`, 'original version' FROM job WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data


	
//Create query

$qry="UPDATE `job` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$job_contact_email', `phone`='$job_phone', `name`='$job_person', `website`='$job_website',  `details`='$job_details' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `job_edits`(`id`,`school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$job_contact_email', '$job_phone', '$job_person', '$job_website',  '$job_check_email', '$job_check_phone', '$job_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}


	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'internship')
{

	$qry_current_data="SELECT * FROM internship_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO internship_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`, 'original version' FROM internship WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data


	
//Create query

$qry="UPDATE `internship` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$internship_contact_email', `phone`='$internship_phone', `name`='$internship_person', `website`='$internship_website', `month`='$internship_month', `day`='$internship_day', `year`='$internship_year', `details`='$internship_details' WHERE id='$id'";

	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `internship_edits`(`id`,`school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$internship_contact_email', '$internship_phone', '$internship_person', '$internship_website', '$internship_month', '$internship_day', '$internship_year',  '$internship_check_email', '$internship_check_phone', '$internship_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}


	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'reu')
{

	$qry_current_data="SELECT * FROM reu_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO reu_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`, 'original version' FROM reu WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data

//Create query

$qry="UPDATE `reu` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$reu_contact_email', `phone`='$reu_phone', `name`='$reu_person', `website`='$reu_website', `month`='$reu_month', `day`='$reu_day', `year`='$reu_year', `details`='$reu_details' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `reu_edits`(`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `website`, `month`, `day`, `year`, `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$reu_contact_email', '$reu_phone', '$reu_person', '$reu_website', '$reu_month', '$reu_day', '$reu_year',  '$reu_check_email', '$reu_check_phone', '$reu_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}


	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'services')
{

	$qry_current_data="SELECT * FROM services_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO services_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `website`, `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `website`, `check_email`, `check_phone`, `details`, 'original version' FROM services WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data

	
//Create query


$qry="UPDATE `services` SET `school`=ccny', `title`='$title', `category`='$category', `contact_email`='$services_contact_email', `phone`='$services_phone', `name`='$services_person', `amount`='$services_amount', `website`='$services_website', `details`='$services_details' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `service_edits`(`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `website`, `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$services_contact_email', '$services_phone', '$services_person', '$services_amount', '$services_website', '$services_check_email', '$services_check_phone', '$services_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}


	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'tutoring')
{


	$qry_current_data="SELECT * FROM tutoring_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO tutoring_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `check_email`, `check_phone`, `details`, 'original version' FROM tutoring WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data


	
//Create query

$qry="UPDATE `tutoring` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$tutoring_contact_email', `phone`='$tutoring_phone', `name`='$tutoring_person', `details`='$tutoring_details' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `tutoring_edits`(`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$tutoring_contact_email', '$tutoring_phone', '$tutoring_person',   '$tutoring_check_email', '$tutoring_check_phone', '$tutoring_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}


	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'textbooks wanted')
{


	$qry_current_data="SELECT * FROM textbooks_wanted_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO textbooks_wanted_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`, 'original version' FROM textbooks_wanted WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data

	
//Create query

$qry="UPDATE `textbooks_wanted` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$textbooks_wanted_contact_email', `phone`='$textbooks_wanted_phone', `name`='$textbooks_wanted_person', `amount`='$textbooks_wanted_amount', `details`='$textbooks_wanted_details' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `textbooks_wanted_edits`(`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$textbooks_wanted_contact_email', '$textbooks_wanted_phone', '$textbooks_wanted_person',  '$textbooks_wanted_amount',  '$textbooks_wanted_check_email', '$textbooks_wanted_check_phone', '$textbooks_wanted_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}


	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}

else if($category == 'textbooks for sale')
{


	$qry_current_data="SELECT * FROM textbooks_for_sale_edits WHERE id='$id'";
	$result_current_data=mysql_query($qry_current_data);
	if($result_current_data)
	{
		if(mysql_num_rows($result_current_data) == 0)
		{

			$qry_copy="INSERT INTO textbooks_for_sale_edits (`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`, `info`) SELECT `id`, `school`, '".$_SESSION['SESS_EMAILADDRESS']."', `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`, 'original version' FROM textbooks_for_sale WHERE id='$id'";
			$result_copy=mysql_query($qry_copy);
			if(! $result_copy)
			{
			die("copy query failed!");
			}

		}

	}


//end of new data

	
//Create query

$qry="UPDATE `textbooks_for_sale` SET `school`='ccny', `title`='$title', `category`='$category', `contact_email`='$textbooks_for_sale_contact_email', `phone`='$textbooks_for_sale_phone', `name`='$textbooks_for_sale_person', `amount`='$textbooks_for_sale_amount', `details`='$textbooks_for_sale_details' WHERE id='$id'";


	$result=@mysql_query($qry);
	if($result)
	{

//Create query
$qry_edit="INSERT INTO `textbooks_for_sale_edits`(`id`, `school`, `edited_by`, `title`, `category`, `contact_email`, `phone`, `name`, `amount`, `check_email`, `check_phone`, `details`,`info`) 
VALUES ('$id', 'ccny', '".$_SESSION['SESS_EMAILADDRESS']."', '$title', '$category', '$textbooks_for_sale_contact_email', '$textbooks_for_sale_phone', '$textbooks_for_sale_person',  '$textbooks_for_sale_amount',  '$textbooks_for_sale_check_email', '$textbooks_for_sale_check_phone', '$textbooks_for_sale_details', 'edited version')"; 


	$result_edit=@mysql_query($qry_edit);
	if(! $result_edit)
	{
	die("query failed32");
	}


	header("location: events2.php");
	exit();
	}
	else
	{
	die("query failed32");
	}

}






			

?>
