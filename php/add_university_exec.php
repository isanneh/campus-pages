<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('connect.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}

	
	//Sanitize the POST values
	$name = clean($_POST['university']);
	$address = clean($_POST['address']);
	$phone = clean($_POST['phone']);
	$state = clean($_POST['state']);
	$zipcode = clean($_POST['zipcode']);
	
	//Input Validations
	if($name == '') {
		$errmsg_arr[] = 'College/University name missing';
		$errflag = true;
	}


	
	if($address == '') {
		$errmsg_arr[] = 'Address name missing';
		$errflag = true;
	}

	if($phone == '') {
		$errmsg_arr[] = 'Zipcode missing';
		$errflag = true;
	}

	if($zipcode == '') {
		$errmsg_arr[] = 'Phone missing';
		$errflag = true;
	}

	if($state == 'choose state') {
		$errmsg_arr[] = 'State missing';
		$errflag = true;
	}

	//If there are input validations, redirect back to the  form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: add_university.php");
		exit();
	}
	

	//Create INSERT query
	$qry = "INSERT INTO colleges(name, address, state, zipcode, phone) 
	VALUES('$name','$address', '$state', '$zipcode', '$phone')";
	$result = @mysql_query($qry);
	
	//Check whether the query was successful or not
	if($result) {
			$_SESSION['SESS_college'] = $name;
		header("location: add_school.php");
		exit();
	}else {
		die("Query failed");
	}
?>
